<?php require('data/dbcon.php'); ?>
<?php require('data/functions.php'); ?>
<?php
// Leave all the code below and above this or the guestbook will not work.
// DO NOT EDIT UNLESS YOU KNOW WHAT YOUR DOING!
session_start();
$error = null;
$sid = session_id();
if( isset($_POST['addpost']) ) {
	
	// SPAM check
	if( !isset($_SESSION['secret']) || !isset($_POST['submission_id']) || ($_SESSION['secret'] != $_POST['submission_id']) ) {
		$addtospamcount = addSPAM();
		
		// This is the SPAM message that will display
		$error = "<p><span class=\"highlight_red\">" . SpamMSG . "</span></p>";
		//version 2.1 added unset
		unset($_SESSION['secret']);
		unset($_COOKIE['PHPSESSID']);
	}else{
		// Post to the database if it's not SPAM!
		$add = addPost($_POST, false);
		if( $add == FALSE ) {
			// Check for an error
			$error = "<p><span class=\"highlight_red\">" . GenMGS . "</span></p>";
			$signal = "false";
			unset($_SESSION['secret']);
			unset($_COOKIE['PHPSESSID']);
		}else{
			// No Error added to database and saved
			$error = "<p id=\"post" . $sid . "\" class=\"fade\"><span class=\"highlight\">" . SuccMSG . "</span></p>";
			$signal = "true";
			unset($_SESSION['secret']);
			unset($_COOKIE['PHPSESSID']);
		}
		
	}

	
}
// Part of the SPAM protection DO NOT REMOVE
$subission_id = md5(uniqid(rand(), true));
$_SESSION['secret'] = $subission_id;
// START HTML below you may edit
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!--You can restyle this guestbook in ANY way just us a style sheet!-->
<style type="text/css">@import url('css/body.css'); </style>

<!--KEEP THE FOLLOWING JAVASCRIPTS IN PLACE OR IT WILL NOT WORK RIGHT...-->
<script type="text/javascript" src="js/prototypejs.js"></script>
<script type="text/javascript" src="js/scriptaculous.js"></script>

<title>My Guestbook from Winn.ws!</title>
</head>

<body>
<h1>Winn Guestbook <?php echo $version;?></h1>
<p>Great guestbook from <a href="http://winn.ws">Winn.ws</a></p>
<?php echo $error; ?>



<!--This call gets all your approved posts, i would not change this or delete it.-->
<!-- Edit the look and feel of your posts by editing the functions.php file -->
<?php if(empty($_GET['p'])) {?>
	<h3>Sorry, no posts here!</h3>
	<p>Find this to be an error? Contact the admin of this site.</p>
<?php }else{ ?>
	<?php $post = $API->GetPostByID($_GET['p']); ?>
	<h3>From: <?php echo $post['name']; ?></h3>
	<p><small><?php echo date("l, F j, Y - g:ia", strtotime($post['dateadded'])); ?></small></p>
	<p style="border-bottom:1px solid #333; padding: 0 0 10px 0;"><?php echo $post['post']; ?></p>
<?php } ?>


<p id="copy">&copy; Winn Guestbook <?php echo $version;?>, <a href="http://winn.ws" target="_blank">Winn.ws</a></p>
<?php if(GOOGLE_ANALYTICS != 'none'){?>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("<?php echo GOOGLE_ANALYTICS;?>");
pageTracker._trackPageview();
</script>
<?php } ?>
</body>
</html>