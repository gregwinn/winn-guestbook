<?php
function curPageURL() {
	$pageURL = 'http';
	if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	$pageURL .= "://";
	if (($_SERVER["SERVER_PORT"] != "80") || ($_SERVER["SERVER_PORT"] != "8080")) {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}

function validateQueryString ( $queryString ) {
	if ( !preg_match ("/^[a-zA-Z]+[:\/\/]+[A-Za-z0-9\-_]+\\.+[A-Za-z0-9\.\/%&=\?\-_]+$/i", $queryString ) ) {
		return false;
	}
	return true;
}
$qString = curPageURL();
if (!validateQueryString($qString)) {
	echo "This page can not be viewed right now. Contact the site owner if you find this to be an error. <a href='/'>Homepage</a>";
	exit;
}
?>