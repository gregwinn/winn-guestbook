/**
 * @author Greg Winn
 */
$(document).ready(function(){
	$(".view_full_post").bind('click',function(){
		postid = $(this).attr('title');
		$("#viewFull_post_loader").load('inc/viewPosts_modal.php', {p: postid});
		$("#viewFull_post").dialog("open");
	});
	$("#viewFull_post").dialog({
		bgiframe: true,
		modal: true,
		height:500,
		width:450,
		autoOpen: false,
		buttons: {
			'Close':function() {
				$(this).dialog('close');
			}
		}
	});
});
