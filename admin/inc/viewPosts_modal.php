<?php require('../../data/dbcon.php'); ?>
<?php require('../../data/functions.php'); ?>
<?php
$post = $API->GetPostByID($_POST['p']);
?>
<h3>Posted By: <?php echo $post['name']; ?></h3>
<p><small><?php echo date("l, F j, Y - g:ia", strtotime($post['dateadded'])); ?></small></p>
<p><?php echo $post['post']; ?></p>