<?php require('../../data/dbcon.php'); ?>
<?php require('../../data/functions.php'); ?>
<?php
session_start();
$UserChk = checkUser($_COOKIE['winnguestbook_u'], $_COOKIE['winnguestbook_auth']);
if( $UserChk == FALSE ) {
	header("location: index.php");
}
$post = $API->GetPostByID($_POST['p']);
?>
<h3>Posted By: <?php echo $post['name']; ?></h3>
<p><small><?php echo date("l, F j, Y - g:ia", strtotime($post['dateadded'])); ?></small></p>
<input type="hidden" value="<?php echo $post['id']; ?>" id="EditPost_id">
<textarea id="EditPost_content" style="width:400px; height:150px; font-family:Arial, Helvetica, sans-serif; font-size:12px; padding:3px;" name="content"><?php echo $post['post']; ?></textarea>