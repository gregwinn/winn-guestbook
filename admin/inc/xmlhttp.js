// JavaScript Document
var xmlhttp = false;
// Check for IE
try{
	xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
}catch (e) {
	try{
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}catch (E) {
		xmlhttp = false;
	}
		
}

// for NON-IE
if( !xmlhttp && typeof XMLHttpRequest != 'undefined') {
	xmlhttp = new XMLHttpRequest();
}


function processajax(servPage, obj, getOrpost, str) {
	
	xmlhttp = getxmlhttp();
	if( getOrpost == "get" ) {
		xmlhttp.open("GET", serverPage);
		xmlhttp.onreadystatechange = function(){
			if( xmlhttp.readyState == 4 && xmlhttp.status == 200 ) {
				obj.innerHTML = xmlhttp.responseText;
			}
		}
		
		xmlhttp.send(null);
	}else{
		xmlhttp.open("POST", serverPage, true);
		xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencode; charset=UTF-8");
		xmlhttp.onreadystatechange = function(){
			if( xmlhttp.readyState == 4 && xmlhttp.status == 200 ) {
				obj.innerHTML = xmlhttp.responseText;
			}
		}
		
		xmlhttp.send(null);
	}
	
}